
#include <getopt.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "mpi.h"

#include "ppp_pnm.h"


int np, self;
bool debugOutput;
int N = 40;
double k = 30;
double dt = 0.1;
double epsilon = 0.005;
int maxcolor;

/*
 * myPart (and myNewPart) has one row above and one row below the
 * actuall rows which are computed by the local process (as
 * Sobel requires values from the neighbouring rows).
 */
uint8_t *myPart;
uint8_t *myNewPart;
int myRows;

int *counts, *displs;

double sobelC;


/* Return seconds passed since midnight on 1970-01-01 */
static double seconds() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
}


/*
 * Check that a color value is in the range 0..maxcolor
 */
inline static int range(double v) {
    int vv = lrint(v);
    return (vv < 0 ? 0 : (vv > maxcolor ? maxcolor : vv));
}

/*
 * Out of memory handler.
 */
void Oom(void) {
    fprintf(stderr, "Out of memory on processor %d\n", self);
    MPI_Abort(MPI_COMM_WORLD, 1);
}

/*
 * Put zeros in the first row in process 0 and
 * in the last row in process np-1.
 */
void prepare_myPart(int columns) {
    int x;
    if (self == 0) {
	for (x=0; x<columns; x++)
	    myPart[x] = 0;
    }
    if (self == np-1) {
	int mr1 = myRows+1;
	for (x=0; x<columns; x++)
	    myPart[mr1 * columns + x] = 0;
    }
}

/*
 * Collect the pieces of the output image.
 */
void collect(int columns) {
    void *sendbuf = self == 0 ? MPI_IN_PLACE : myNewPart;
    MPI_Gatherv(sendbuf, counts[self], MPI_UINT8_T,
                myNewPart, counts, displs, MPI_UINT8_T, 0, MPI_COMM_WORLD);
}


/*
 * New pixel value according to Sobel for given values of sx and sy.
 */
inline static int T(int sx, int sy) {
    int v = lrint(sobelC * hypot(sx,sy));
    return v > maxcolor ? maxcolor : v;
}



double eta (double input) {
  double s = input / (sqrt(2) * k);
  double result = (1 / sqrt(2)) * s * exp(-(s * s) / 2);
    return result;
}

double phi(double input) {
  double x = input/k;
  return x * exp(-(x*x)/2);
}

/*
 * Naive sequential VCD implementation.
 */
void vcdNaive(const int columns) {
    int rows = myRows;
    uint8_t (*image)[columns] = (uint8_t (*)[columns]) &myPart[columns];
    
    inline double S(int c, int r) {
      return c >= 0 && c < columns ? ((double) image[r][c])/255 : 0;
    }
    
    inline double delta(int x, int y) {
      double deltaXY = phi(S(x+1, y) - S(x, y)) - phi(S(x, y) - S(x-1, y)) 
		     + phi(S(x, y+1) - S(x, y)) - phi(S(x, y) - S(x, y-1)) 
		     + eta(S(x+1, y+1) - S(x,y)) - eta(S(x, y) - S(x-1, y-1))
		     + eta(S(x-1, y+1) - S(x,y)) - eta(S(x,y) - S(x+1, y-1));
      return deltaXY;
    }

    bool biggereps = false;
    int i = 0;
      while (!biggereps && i < N) {
	biggereps = true;
	i++;

    for (int y=0; y<rows; ++y) {
	for (int x=0; x<columns; ++x) {
	  double deltaXY = delta(x,y);
	  if (y > 0 && y < rows-1 && x > 0 && x < columns-1 && fabs(deltaXY) > epsilon) {
	    biggereps = false;
	  }
	  double t =  image[y][x] + k * dt * deltaXY * 255;
	  uint8_t t2 = (uint8_t) llround(t);
	  uint8_t t3 = t2 > 255 ? maxcolor : t2;
	  myNewPart[y*columns+x] = t3 < 0 ? 0 : t3;
	}
    }
	for (int s = 0; s < rows; s++) {
	 for (int t = 0; t < columns; t++) {
		image[s][t] = myNewPart[s * columns + t];
	 }
	}
    }    
}




void vcd(const int columns) {
    uint8_t (*image)[columns] = (uint8_t (*)[columns]) &myPart[columns];
    inline int S(int c, int r) { return image[r][c]; }

    inline double delta(int x, int y) {
      double deltaXY = phi(S(x+1, y) - S(x, y)) - phi(S(x, y) - S(x-1, y)) 
		     + phi(S(x, y+1) - S(x, y)) - phi(S(x, y) - S(x, y-1)) 
		     + eta(S(x+1, y+1) - S(x,y)) - eta(S(x, y) - S(x-1, y-1))
		     + eta(S(x-1, y+1) - S(x,y)) - eta(S(x,y) - S(x+1, y-1));
      return deltaXY;
    }

#pragma omp parallel for
    for (int y=0; y<myRows; ++y) {
       

	for (int x=0; x<columns; ++x) {
	  double deltaXY = delta(x,y);
	  /*if (y > 0 && y < myRows-1 && x > 0 && x < columns-1 && fabs(deltaXY) > epsilon) {
	    biggereps = false;
	  } */
	  double t =  image[y][x] + k * dt * deltaXY * 255;
	  uint8_t t2 = (uint8_t) llround(t);
	  uint8_t t3 = t2 > 255 ? maxcolor : t2;
	  myNewPart[y*columns+x] = t3 < 0 ? 0 : t3;
	}
    }
}

/*
 * Callback function for ppp_pnm_load.
 * We determine the part of the image to be processed
 * by the local process and load one additional row
 * above and below this part as the Sobel operator needs
 * data from these two additional rows for its computations.
 */
uint8_t *partFn(enum pnm_kind kind, int rows, int columns,
                int *offset, int *length)
{
    int i;

    if (kind != PNM_KIND_PGM)
	return NULL;

    if (rows < np) {
	if (self == 0)
	    fprintf(stderr, "Cannot run with fewer rows in the image "
		    "than processors.\n");
	return NULL;
    }

    counts = malloc(2 * np * sizeof(int));
    if (counts == NULL)
	Oom();
    displs = &counts[np];

    /*
     * The number of rows need not be a multiple of
     * np. Therefore, the first  rows%np  processes get
     *    ceil(rows/np)
     * rows, and the remaining processes get
     *    floor(rows/np)
     * rows.
     */
    displs[0] = 0;
    counts[0] = (rows/np + (0 < rows%np ? 1 : 0)) * columns;
    for (i=1; i<np; i++) {
	counts[i] = (rows/np + (i < rows%np ? 1 : 0)) * columns;
	displs[i] = displs[i-1] + counts[i-1];
    }

    myRows = counts[self] / columns;

    /*
     * myPart has two additional rows, one at the top, one
     * at the bottom of the local part of image.
     */
    myPart = malloc((myRows+2) * columns * sizeof(int));
    if (myPart == NULL) {
	free(displs);
	Oom();
    }

    /*
     * Space for the result image part without additional
     * rows at the top and bottom.
     * On processor 0, we reserve space for the whole image
     * so we can collect the parts with MPI_Gatherv into
     * this space.
     */
    myNewPart = malloc((self == 0 ? rows : myRows) * columns * sizeof(int));
    if (myNewPart == NULL) {
	free(displs);
	free(myPart);
	Oom();
    }

    /*
     * Offset and length of the part of the image to load
     * in the local process, including the additional
     * row at the top and/or the bottom.
     */
    *offset = self == 0 ? 0 : (displs[self]-columns);
    if (np == 1)
	*length = myRows * columns;
    else
	*length = (myRows + (self == 0 || self == np-1 ? 1 : 2)) * columns;

    if (debugOutput) {
        fprintf(stderr, "self=%d: *offset = %d, *length = %d\n",
                self, *offset, *length);
    }

    /* Add zeros in top row in process 0 and in bottom row in process np-1. */
    prepare_myPart(columns);

    return (self == 0 ? &myPart[columns] : myPart);
}

void usage(const char *progname) {
    if (self == 0) {
	fprintf(stderr,
		"USAGE: %s -i input.pgm [-o output.pgm] [-c coeff] "
		"[-n] [-r] [-t] [-d] [-h]\n"
		"  coeff  sobel coefficient\n"
		"  -n     use naive Sobel operator\n"
		"  -d     give some debug output\n"
		"  -h     print this help\n",
		progname);
    }
}

int main(int argc, char *argv[])
{
    int option;
    uint8_t *image;
    char *filename, *outfilename;
    enum pnm_kind kind;
    int rows, columns;
    bool naive = false;     /* use naive implementation */
    double time_start, time_loaded;
    double time_computed=0.0, time_finished;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &self);

    filename = NULL;
    outfilename = NULL;
    sobelC = 0.9;
    debugOutput = false;

    while ((option = getopt(argc,argv,"i:o:c:ndh")) != -1) {
	switch(option) {
	case 'i': filename = optarg; break;
	case 'o': outfilename = optarg; break;
	case 'c': sobelC = atof(optarg); break;
	case 'n': naive = true; break;
	case 'd': debugOutput = true; break;
	case 'h':
	    usage(argv[0]);
	    MPI_Finalize();
	    return 0;
	default:
	    usage(argv[0]);
	    MPI_Finalize();
	    return 1;
	}
    }
    if (filename == NULL) {
        usage(argv[0]);
        MPI_Finalize();
	return 1;
    }

    time_start = seconds();
    image = ppp_pnm_read_part(filename, &kind, &rows, &columns, &maxcolor,
			      partFn);
    if (image == NULL) {
	fprintf(stderr, "Could not load image from file '%s'"
		" on processor %d.\n", filename, self);
	MPI_Abort(MPI_COMM_WORLD, 1);
    } else if (kind != PNM_KIND_PGM) {
	if (self == 0)
	    fprintf(stderr, "Image is not a \"portable graymap.\"\n");
	MPI_Abort(MPI_COMM_WORLD, 1);
    }

    time_loaded = seconds();
    
    if (naive)
        vcdNaive(columns);
    else
        vcd(columns);

    time_computed = seconds();

    collect(columns);
    
    time_finished = seconds();

    if (self == 0) {
	if (outfilename != NULL)
	    ppp_pnm_write(outfilename, kind, rows, columns, maxcolor,
			  myNewPart);

	printf("Times:\n");
	printf("  Load:    %.6f s\n", time_loaded-time_start);
        printf("  Compute: %.6f s\n", time_computed-time_loaded);
        printf("  Collect: %.6f s\n", time_finished-time_computed);
	printf("  TOTAL:   %.6f s\n", time_finished-time_start);
    }

    free(myPart);
    MPI_Finalize();

    return 0;
}
