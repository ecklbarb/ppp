/*
 * Sobel Operator
 *
 * Compile with (as one command):
 *
 *   mpicc -std=gnu99 -Wall -O3 -fopenmp -o sobel sobel.c
 *      -I/home/ppp2013/ppp_pnm -L/home/ppp2013/ppp_pnm -lppp_pnm -lm
 *
 * Note that the code uses C99 and GNU extensions.
 */
#include <getopt.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "mpi.h"

#include "ppp_pnm.h"


/*
 * Sobel Operator
 *
 * The Sobel operator detects edges in a grayscale image by
 * computing a new pixel value t(x,y) for each pixel
 * from the old pixel values s(x,y) by the following formula:
 *
 *   s_x(x,y) =   s(x-1,y-1) + 2*s(x,y-1) + s(x+1,y-1)
 *              - s(x-1,y+1) - 2*s(x,y+1) - s(x+1,y+1)
 *
 *   s_y(x,y) =   s(x-1,y-1) + 2*s(x-1,y) + s(x-1,y+1)
 *              - s(x+1,y-1) - 2*s(x+1,y) - s(x+1,y+1)
 *
 *   t(x,y) = c * sqrt( s_x(x,y)^2 + s_y(x,y)^2 )
 *
 * For the parameter 'c' the value 0.9 is usually a good choice.
 * Values of pixels outside the input image (s(-1,-1) etc.) are
 * considered to be 0.
 */


int np, self;
bool debugOutput;
int N = 40;
double k = 30;
double dt = 0.1;
double epsilon = 0.005;
int maxcolor;
bool biggereps=true;
/*
 * myPart (and myNewPart) has one row above and one row below the
 * actuall rows which are computed by the local process (as
 * Sobel requires values from the neighbouring rows).
 */
uint8_t *myPart;
uint8_t *myNewPart;
double *doublearray;
int myRows;

int *counts, *displs;

double sobelC;


/* Return seconds passed since midnight on 1970-01-01 */
static double seconds() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
}


/*
 * Check that a color value is in the range 0..maxcolor
 */
inline static int range(double v) {
    int vv = lrint(v);
    return (vv < 0 ? 0 : (vv > maxcolor ? maxcolor : vv));
}

/*
 * Out of memory handler.
 */
void Oom(void) {
    fprintf(stderr, "Out of memory on processor %d\n", self);
    MPI_Abort(MPI_COMM_WORLD, 1);
}

/*
 * Put zeros in the first row in process 0 and
 * in the last row in process np-1.
 */
void prepare_myPart(int columns) {
    int x;
    if (self == 0) {
    for (x=0; x<columns; x++)
        myPart[x] = 0;
    }
    if (self == np-1) {
    int mr1 = myRows+1;
    for (x=0; x<columns; x++)
        myPart[mr1 * columns + x] = 0;
    }
}

/*
 * Collect the pieces of the output image.
 */
void collect(int columns) {
    void *sendbuf = self == 0 ? MPI_IN_PLACE : myNewPart;
    MPI_Gatherv(sendbuf, counts[self], MPI_UINT8_T,
                myNewPart, counts, displs, MPI_UINT8_T, 0, MPI_COMM_WORLD);
}


/*
 * New pixel value according to Sobel for given values of sx and sy.
 */
inline static int T(int sx, int sy) {
    int v = lrint(sobelC * hypot(sx,sy));
    return v > maxcolor ? maxcolor : v;
}



double eta (double input) {
  double s = input / (sqrt(2) * k);
  double result = (1 / sqrt(2)) * s * exp(-(s * s) / 2);
    return result;
}

double phi(double input) {
  double x = input/k;
  return x * exp(-(x*x)/2);
}

/*
 * Naive sequential VCD implementation.
 */
void vcdNaive(const int columns) {
    int rows = myRows;

    uint8_t (*image)[columns] = (uint8_t (*)[columns]) &myPart[columns];

    double *doublearray = (double *)malloc(rows*columns*sizeof(double));
    inline double S(int c, int r) {
      return c >= 0 && c < columns ? ( doublearray[r*columns+c]) : 0;
    }
    bool* changeArray;
    changeArray = (bool *)malloc(rows * columns * sizeof(bool));

    inline bool B(int x, int y) {
      return x >= 0 && x < columns-1 && y >= 0 && y < rows-1 ? changeArray[y*columns+x] : false;
    }
    inline double delta(int x, int y) {
      double deltaXY = phi(S(x+1, y) - S(x, y))  - phi(S(x, y) - S(x-1, y))
             + phi(S(x, y+1) - S(x, y))  - phi(S(x, y) - S(x, y-1))
             + eta(S(x+1, y+1) - S(x,y)) - eta(S(x, y) - S(x-1, y-1))
             + eta(S(x-1, y+1) - S(x,y)) - eta(S(x,y) - S(x+1, y-1));
      return deltaXY;
    }

    for (int y=0; y<rows; ++y) {
      for (int x=0; x<columns; ++x) {
    changeArray[y*columns+x] = true;
      }
    }
    biggereps = false;
    int i = 0;
    double *temp = (double *)malloc(rows*columns*sizeof(double));
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
    doublearray[i*columns+j] = ((double)image[i][j])/255;
      }
    }
    while (!biggereps && i < N) {
    biggereps = true;
    i++;

    for (int y=0; y<rows; ++y) {
    for (int x=0; x<columns; ++x) {
if (B(x-1,y-1) && B(x,y-1) && B(x+1,y-1) && B(x-1,y) && B(x,y) && B(x+1,y) && B(x-1,y+1) && B(x,y+1) && B(x+1,y+1)) {
      double deltaXY = delta(x,y);

        changeArray[y*columns+x] = (deltaXY!=0);
      if (y > 0 && y < rows-1 && x > 0 && x < columns-1 && fabs(deltaXY) > epsilon) {
        biggereps = false;
      }
      double t = doublearray[y*columns+x] + k * dt * deltaXY;

      temp[y*columns+x] = t;
 }
    }
    }
    printf("Im %d-ten Durchlauf: Wert %f \n", i, doublearray[0]);
    for (int s = 0; s < rows; s++) {
     for (int t = 0; t < columns; t++) {
    doublearray[s*columns+t] = temp[s * columns + t];
    }
}
    }

    for (int i = 0; i < rows; i++) {
      uint8_t t1;
      for(int j = 0; j < columns; j++) {
    t1 = (int) round((doublearray[i*columns+j])*255);
    uint8_t t2 = t1 > 255 ? 255 : t1;
    myNewPart[i*columns+j] = t2 < 0 ? 0 : t2;
      }
    }
    printf("Im Image: Wert %d, im doublearray %f \n", myNewPart[0], doublearray[0]);

}



/*
 * Sobel with unrolling of the first and last iteration
 * of the loop on x to avoid the case distinctions
 * in the innermost loop.
 */
void sobel(const int columns) {
    uint8_t (*image)[columns] = (uint8_t (*)[columns]) &myPart[columns];
    inline int S(int c, int r) { return image[r][c]; }

#pragma omp parallel for
    for (int y=0; y<myRows; ++y) {
        int sx, sy;

    // x == 0
    sx = 2*S(0,y-1) + S(1,y-1) - 2*S(0,y+1) - S(1,y+1);
    sy = - S(1,y-1) - 2*S(1,y) - S(1,y+1);
    myNewPart[y*columns] = T(sx,sy);

    for (int x=1; x<columns-1; ++x) {
        sx = S(x-1,y-1) + 2*S(x,y-1) + S(x+1,y-1)
        -S(x-1,y+1) - 2*S(x,y+1) - S(x+1,y+1);
        sy = S(x-1,y-1) + 2*S(x-1,y) + S(x-1,y+1)
            -S(x+1,y-1) - 2*S(x+1,y) - S(x+1,y+1);
        myNewPart[y*columns+x] = T(sx,sy);
    }

    // x == columns-1
    sx = S(columns-2,y-1) + 2*S(columns-1,y-1)
        -S(columns-2,y+1) - 2*S(columns-1,y+1);
    sy = S(columns-2,y-1) + 2*S(columns-2,y) + S(columns-2,y+1);
    myNewPart[y*columns+columns-1] = T(sx,sy);
    }
}

void vcd (const int columns, int iteration) {
  
  inline double S(int c, int r) {
      return c >= 0 && c < columns ? ( doublearray[r*columns+c]) : 0;
   }

  //Berechnen der Werte zwischen 0 und 1
  #pragma omp parallel 
  {
  if (iteration == 0) {
    doublearray = (double *) malloc(columns*myRows*sizeof(double));

    #pragma omp for
    for (int i = 0; i < myRows; i++) {
      for (int j = 0; j < columns; j++) {
    	int k;
    	if (self == 0 && i < myRows -1) {
    	  k = i+1;
    	  if (i == 0) {
    	    doublearray[j]= 0;
    	  }
    	} else {
     	 k = i;
    	}

    	if (self == np-1 && i == myRows -1) {
      	 doublearray[i*columns+j]= 0;
    	} else {
      	 doublearray[k*columns+j] = ((double) myPart[i*columns+j])/255;
    	}
      }
    }
  }


  //Berechnung vcd
  double *temp = (double *)malloc(myRows*columns*sizeof(double));
  for(int i = 0; i < columns; i++) {
    temp[i] = 0;
    temp[(myRows-1)*columns +i] = 0;
  }

  biggereps = false;
  for (int y = 1; y < myRows-1; ++y) {
    double deltaXY;
    for (int x = 0; x < columns; ++x) {

      deltaXY = phi(S(x+1, y) - S(x, y))  - phi(S(x, y) - S(x-1, y))
             + phi(S(x, y+1) - S(x, y))  - phi(S(x, y) - S(x, y-1))
             + eta(S(x+1, y+1) - S(x,y)) - eta(S(x, y) - S(x-1, y-1))
             + eta(S(x-1, y+1) - S(x,y)) - eta(S(x,y) - S(x+1, y-1));

     int limittop = self == 0 ? 1 : 0;
     int limitbottom = self == np-1 ? myRows-2 : myRows-1;
     if (y > limittop && y < limitbottom && x > 0 && x < columns-1 && fabs(deltaXY) > epsilon) {
        biggereps = true;
     }
     double t = doublearray[y*columns+x] + k * dt * deltaXY;

    temp[y*columns+x] = t;

    }
    //printf("%f|", deltaXY);
  }
  // printf("\n biggereps in process %d: %i|||\n", self, biggereps);
  for(int i = 0; i < myRows; i++) {
    for(int j = 0; j < columns; j++) {
      doublearray[i*columns+j] = temp[i*columns+j];
      //Optimierung: Während Rechenschleife schreiben mit 1 Zeile & 2 Spalten zurückversetzt
    }
  }

}

/*
 * Callback function for ppp_pnm_load.
 * We determine the part of the image to be processed
 * by the local process and load one additional row
 * above and below this part as the Sobel operator needs
 * data from these two additional rows for its computations.
 */
uint8_t *partFn(enum pnm_kind kind, int rows, int columns,
                int *offset, int *length)
{
    int i;

    if (kind != PNM_KIND_PGM)
    return NULL;

    if (rows < np) {
    if (self == 0)
        fprintf(stderr, "Cannot run with fewer rows in the image "
            "than processors.\n");
    return NULL;
    }

    counts = malloc(2 * np * sizeof(int));
    if (counts == NULL)
    Oom();
    displs = &counts[np];

    /*
     * The number of rows need not be a multiple of
     * np. Therefore, the first  rows%np  processes get
     *    ceil(rows/np)
     * rows, and the remaining processes get
     *    floor(rows/np)
     * rows.
     */
    displs[0] = 0;
    counts[0] = (rows/np + (0 < rows%np ? 1 : 0)) * columns;
    for (i=1; i<np; i++) {
    counts[i] = (rows/np + (i < rows%np ? 1 : 0)) * columns;
    displs[i] = displs[i-1] + counts[i-1];
    }

    myRows = counts[self] / columns;

    /*
     * myPart has two additional rows, one at the top, one
     * at the bottom of the local part of image.
     */
    myPart = malloc((myRows+2) * columns * sizeof(int));
    if (myPart == NULL) {
    free(displs);
    Oom();
    }

    /*
     * Space for the result image part without additional
     * rows at the top and bottom.
     * On processor 0, we reserve space for the whole image
     * so we can collect the parts with MPI_Gatherv into
     * this space.
     */
    myNewPart = malloc((self == 0 ? rows : myRows) * columns * sizeof(int));
    if (myNewPart == NULL) {
    free(displs);
    free(myPart);
    Oom();
    }

    /*
     * Offset and length of the part of the image to load
     * in the local process, including the additional
     * row at the top and/or the bottom.
     */
    *offset = self == 0 ? 0 : (displs[self]-columns);
    if (np == 1)
    *length = myRows * columns;
    else
    *length = (myRows + (self == 0 || self == np-1 ? 1 : 2)) * columns;

    if (debugOutput) {
        fprintf(stderr, "self=%d: *offset = %d, *length = %d\n",
                self, *offset, *length);
    }

    /* Add zeros in top row in process 0 and in bottom row in process np-1. */
    prepare_myPart(columns);

    return (self == 0 ? &myPart[columns] : myPart);
}

void usage(const char *progname) {
    if (self == 0) {
    fprintf(stderr,
        "USAGE: %s -i input.pgm [-o output.pgm] [-c coeff] "
        "[-n] [-r] [-t] [-d] [-h]\n"
        "  coeff  sobel coefficient\n"
        "  -n     use naive Sobel operator\n"
        "  -d     give some debug output\n"
        "  -h     print this help\n",
        progname);
    }
}

int main(int argc, char *argv[])
{
    int option;
    uint8_t *image = 0;
    char *filename, *outfilename;
    enum pnm_kind kind;
    int rows, columns;
    bool naive = false;     /* use naive implementation */
    bool use_sobel = false;     /*use sobel after vcd */
    double time_start;
    double time_loaded = 0;
    double time_computed=0.0, time_finished;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &self);

    filename = NULL;
    outfilename = NULL;
    sobelC = 0.9;
    debugOutput = false;

    while ((option = getopt(argc,argv,"i:o:c:ndsh")) != -1) {
    switch(option) {
    case 'i': filename = optarg; break;
    case 'o': outfilename = optarg; break;
    case 'c': sobelC = atof(optarg); break;
    case 'n': naive = true; break;
    case 'd': debugOutput = true; break;
    case 's': use_sobel = true; break;
    case 'h':
        usage(argv[0]);
        MPI_Finalize();
        return 0;
    default:
        usage(argv[0]);
        MPI_Finalize();
        return 1;
    }
    }
    if (filename == NULL) {
        usage(argv[0]);
        MPI_Finalize();
    return 1;
    }

    time_start = seconds();
    if (self == 0) {

	if (naive) {
	  image = ppp_pnm_read_part(filename, &kind, &rows, &columns, &maxcolor,
			      partFn);
	} else {
      image = ppp_pnm_read(filename, &kind, &rows, &columns, &maxcolor);
}
      if (image == NULL) {
    fprintf(stderr, "Could not load image from file '%s'"
        " on processor %d.\n", filename, self);
    MPI_Abort(MPI_COMM_WORLD, 1);
      } else if (kind != PNM_KIND_PGM) {
    if (self == 0)
      fprintf(stderr, "Image is not a \"portable graymap.\"\n");
    MPI_Abort(MPI_COMM_WORLD, 1);
      }

      time_loaded = seconds();
    fprintf(stderr,"size: %d x %d\n", rows, columns);
    }
    if (naive) {
      vcdNaive(columns);
    }
    else {
      MPI_Bcast(&rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
      MPI_Bcast(&columns, 1, MPI_INT, 0, MPI_COMM_WORLD);

      int *offsets = (int *)malloc(np*sizeof(int));
      int *sizes = (int *)malloc(np*sizeof(int));

      //Blockzeilen berechnen (aufrunden)
      int blocksize = (int)ceil( ((double)rows)/np );

      myRows = self == np-1 ? blocksize+2 : rows-(np-1)*blocksize+2;
      for(int i = 0; i < np; i++) {
    	sizes[i] = (myRows)*columns;
    	offsets[i] = (blocksize*i - 1)*columns;
      }

      // Offset und Size für den ersten und den letzten Block korrekt berechnen.
      offsets[0] = 0;
      offsets[np-1] = rows*columns-sizes[np-1];
      sizes[0]=(blocksize+1 )*columns;
      sizes[np-1] = (rows-(np-1)*blocksize+1) *columns;

      //Bild verteilen
      myPart = (uint8_t *)malloc(sizes[self]*sizeof(uint8_t));
      fprintf(stderr,"original size mypart: %d\n", (int)(sizes[self]*sizeof(uint8_t)));
      MPI_Scatterv(image, sizes, offsets, MPI_UINT8_T, myPart, sizes[self], MPI_UINT8_T, 0, MPI_COMM_WORLD);

      //Schleife für Iteration
      int r = 0;
      MPI_Request req;
      while (r < N && biggereps) {
    fprintf(stderr, "Iteration %d, biggereps %i\n", r, biggereps);
      //vcd Operator einmal anwenden
    vcd(columns, r);

      /*
    Austausch der Abbruchbedingung
      */
    fprintf(stderr, "biggereps before reduce in process %d : %i\n", self, biggereps);

    MPI_Allreduce(&biggereps, &biggereps, 1, MPI_C_BOOL,MPI_LOR, MPI_COMM_WORLD);

    fprintf(stderr, "biggereps after reduce in process %d: %i\n", self, biggereps);
      /*Randteile austauschen*/
    if (self > 0) {
      MPI_Isend(&doublearray[columns+1], columns, MPI_DOUBLE, self-1, 0, MPI_COMM_WORLD, &req);
      MPI_Recv(&doublearray[0], columns, MPI_DOUBLE, self-1,0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    if (self < np-1) {
      MPI_Isend(&doublearray[(myRows-2) * columns + 1], columns, MPI_DOUBLE, self+1, 0, MPI_COMM_WORLD, &req);
      MPI_Recv(&doublearray[(myRows-1)*columns+1], columns, MPI_DOUBLE, self +1,0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    r++;
      }
       for(int i = 0; i < np; i++) {
     sizes[i] = blocksize*columns;
     offsets[i] = i*blocksize*columns;
     fprintf(stderr,"\nsize: %d, offset: %d ||| ", sizes[i], offsets[i]);
       }
       sizes[np-1] = rows*columns-offsets[np-1];
       fprintf(stderr, " size: %d\n", sizes[np-1]);

       myNewPart = (uint8_t *)malloc(sizes[self]*sizeof(uint8_t));
        for(int i = 1; i < myRows-1; i++) {
          for(int j = 0; j < columns; j++) {
             myNewPart[(i-1)*columns+j] = (int) round(doublearray[i*columns+j]*255);
          }
        }

        fprintf(stderr, "TESTOUTPUT %d, size mypart: %d, size image: %d \n", self, (int)(sizeof(myPart)), rows*columns);

        MPI_Barrier(MPI_COMM_WORLD);
      
        if (self == 0) image = (uint8_t *)malloc(sizeof(uint8_t)*rows*columns*2);
	MPI_Gatherv(myNewPart, sizes[self], MPI_UINT8_T, image, sizes, offsets, MPI_UINT8_T, 0, MPI_COMM_WORLD);
        //MPI_Gatherv(&myNewPart[self != np-1 ? 0: columns], sizes[self], MPI_UINT8_T, image, sizes, offsets, MPI_UINT8_T, 0, MPI_COMM_WORLD);
        fprintf(stderr,"gather complete at process %d\n", self);

    }

    

    time_computed = seconds();

    //collect(columns);

    time_finished = seconds();

    if (self == 0) {
    if (outfilename != NULL) {
	if(naive) 
		ppp_pnm_write(outfilename, kind, rows, columns, maxcolor,
              myNewPart);
	else
        ppp_pnm_write(outfilename, kind, rows, columns, maxcolor,
              image);
    }

	if (use_sobel) {
	image = ppp_pnm_read_part(outfilename, &kind, &rows, &columns, &maxcolor,
			      partFn);
	sobel(columns);
	ppp_pnm_write(outfilename, kind, rows, columns, maxcolor,
			  myNewPart);
    }
    printf("Times:\n");
    printf("  Load:    %.6f s\n", time_loaded-time_start);
        printf("  Compute: %.6f s\n", time_computed-time_loaded);
        printf("  Collect: %.6f s\n", time_finished-time_computed);
    printf("  TOTAL:   %.6f s\n", time_finished-time_start);
    }

    free(myPart);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

    return 0;
}
}

