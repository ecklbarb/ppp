/*
 * Sobel Operator
 *
 * Compile with (as one command):
 *
 *   mpicc -std=gnu99 -Wall -O3 -fopenmp -o sobel sobel.c
 *      -I/home/ppp2013/ppp_pnm -L/home/ppp2013/ppp_pnm -lppp_pnm -lm
 *
 * Note that the code uses C99 and GNU extensions.
 */
#include <getopt.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "mpi.h"

#include "ppp_pnm.h"


/*
 * Sobel Operator
 *
 * The Sobel operator detects edges in a grayscale image by
 * computing a new pixel value t(x,y) for each pixel
 * from the old pixel values s(x,y) by the following formula:
 *
 *   s_x(x,y) =   s(x-1,y-1) + 2*s(x,y-1) + s(x+1,y-1)
 *              - s(x-1,y+1) - 2*s(x,y+1) - s(x+1,y+1)
 *
 *   s_y(x,y) =   s(x-1,y-1) + 2*s(x-1,y) + s(x-1,y+1)
 *              - s(x+1,y-1) - 2*s(x+1,y) - s(x+1,y+1)
 *
 *   t(x,y) = c * sqrt( s_x(x,y)^2 + s_y(x,y)^2 )
 *
 * For the parameter 'c' the value 0.9 is usually a good choice.
 * Values of pixels outside the input image (s(-1,-1) etc.) are
 * considered to be 0.
 */


int np, self;
bool debugOutput;

int maxcolor;

/*
 * myPart (and myNewPart) has one row above and one row below the
 * actuall rows which are computed by the local process (as
 * Sobel requires values from the neighbouring rows).
 */
uint8_t *myPart;
uint8_t *myNewPart;
int myRows;

int *counts, *displs;

double sobelC;


/* Return seconds passed since midnight on 1970-01-01 */
static double seconds() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
}


/*
 * Check that a color value is in the range 0..maxcolor
 */
inline static int range(double v) {
    int vv = lrint(v);
    return (vv < 0 ? 0 : (vv > maxcolor ? maxcolor : vv));
}

/*
 * Out of memory handler.
 */
void Oom(void) {
    fprintf(stderr, "Out of memory on processor %d\n", self);
    MPI_Abort(MPI_COMM_WORLD, 1);
}

/*
 * Put zeros in the first row in process 0 and
 * in the last row in process np-1.
 */
void prepare_myPart(int columns) {
    int x;
    if (self == 0) {
	for (x=0; x<columns; x++)
	    myPart[x] = 0;
    }
    if (self == np-1) {
	int mr1 = myRows+1;
	for (x=0; x<columns; x++)
	    myPart[mr1 * columns + x] = 0;
    }
}

/*
 * Collect the pieces of the output image.
 */
void collect(int columns) {
    void *sendbuf = self == 0 ? MPI_IN_PLACE : myNewPart;
    MPI_Gatherv(sendbuf, counts[self], MPI_UINT8_T,
                myNewPart, counts, displs, MPI_UINT8_T, 0, MPI_COMM_WORLD);
}


/*
 * New pixel value according to Sobel for given values of sx and sy.
 */
inline static int T(int sx, int sy) {
    int v = lrint(sobelC * hypot(sx,sy));
    return v > maxcolor ? maxcolor : v;
}

/*
 * Naive sequential Sobel implementation.
 */
void sobelNaive(const int columns) {
    int rows = myRows;
    /*
     * In C99, we can declare (pointers to) arrays with
     * dynamic size. Here, 'image' points to an array
     * with 'columns' entries. As 'image' is a pointer,
     * it itself can be used as an array (each entry of
     * this array being an array with 'columns' entries), i.e.,
     * 'image' can be used as a two-dimensional array with
     * 'columns' entries in the inner dimension.
     */
    uint8_t (*image)[columns] = (uint8_t (*)[columns]) &myPart[columns];

    /*
     * GCC allows local functions. Without local functions,
     * we would have to use a macro, e.g.,
     *   #define S(c,r) (image[(r)][(c)])
     * to access the pixels conveniently
     * (or use C++11 lambda functions).
     */
    inline int S(int c, int r) {
        return c >= 0 && c < columns ? image[r][c] : 0;
    }

#pragma omp parallel for
    for (int y=0; y<rows; ++y) {
	for (int x=0; x<columns; ++x) {
            int sx, sy;
	    sx = S(x-1,y-1) + 2*S(x,y-1) + S(x+1,y-1)
		-S(x-1,y+1) - 2*S(x,y+1) - S(x+1,y+1);
	    sy = S(x-1,y-1) + 2*S(x-1,y) + S(x-1,y+1)
	        -S(x+1,y-1) - 2*S(x+1,y) - S(x+1,y+1);
	    myNewPart[y*columns+x] = T(sx,sy);
	}
    }    
}

/*
 * Sobel with unrolling of the first and last iteration
 * of the loop on x to avoid the case distinctions
 * in the innermost loop.
 */
void sobel(const int columns) {
    uint8_t (*image)[columns] = (uint8_t (*)[columns]) &myPart[columns];
    inline int S(int c, int r) { return image[r][c]; }

#pragma omp parallel for
    for (int y=0; y<myRows; ++y) {
        int sx, sy;

	// x == 0
	sx = 2*S(0,y-1) + S(1,y-1) - 2*S(0,y+1) - S(1,y+1);
	sy = - S(1,y-1) - 2*S(1,y) - S(1,y+1);
	myNewPart[y*columns] = T(sx,sy);

	for (int x=1; x<columns-1; ++x) {
	    sx = S(x-1,y-1) + 2*S(x,y-1) + S(x+1,y-1)
		-S(x-1,y+1) - 2*S(x,y+1) - S(x+1,y+1);
	    sy = S(x-1,y-1) + 2*S(x-1,y) + S(x-1,y+1)
	        -S(x+1,y-1) - 2*S(x+1,y) - S(x+1,y+1);
	    myNewPart[y*columns+x] = T(sx,sy);
	}

	// x == columns-1
	sx = S(columns-2,y-1) + 2*S(columns-1,y-1)
	    -S(columns-2,y+1) - 2*S(columns-1,y+1);
	sy = S(columns-2,y-1) + 2*S(columns-2,y) + S(columns-2,y+1);
	myNewPart[y*columns+columns-1] = T(sx,sy);
    }
}

/*
 * Callback function for ppp_pnm_load.
 * We determine the part of the image to be processed
 * by the local process and load one additional row
 * above and below this part as the Sobel operator needs
 * data from these two additional rows for its computations.
 */
uint8_t *partFn(enum pnm_kind kind, int rows, int columns,
                int *offset, int *length)
{
    int i;

    if (kind != PNM_KIND_PGM)
	return NULL;

    if (rows < np) {
	if (self == 0)
	    fprintf(stderr, "Cannot run with fewer rows in the image "
		    "than processors.\n");
	return NULL;
    }

    counts = malloc(2 * np * sizeof(int));
    if (counts == NULL)
	Oom();
    displs = &counts[np];

    /*
     * The number of rows need not be a multiple of
     * np. Therefore, the first  rows%np  processes get
     *    ceil(rows/np)
     * rows, and the remaining processes get
     *    floor(rows/np)
     * rows.
     */
    displs[0] = 0;
    counts[0] = (rows/np + (0 < rows%np ? 1 : 0)) * columns;
    for (i=1; i<np; i++) {
	counts[i] = (rows/np + (i < rows%np ? 1 : 0)) * columns;
	displs[i] = displs[i-1] + counts[i-1];
    }

    myRows = counts[self] / columns;

    /*
     * myPart has two additional rows, one at the top, one
     * at the bottom of the local part of image.
     */
    myPart = malloc((myRows+2) * columns * sizeof(int));
    if (myPart == NULL) {
	free(displs);
	Oom();
    }

    /*
     * Space for the result image part without additional
     * rows at the top and bottom.
     * On processor 0, we reserve space for the whole image
     * so we can collect the parts with MPI_Gatherv into
     * this space.
     */
    myNewPart = malloc((self == 0 ? rows : myRows) * columns * sizeof(int));
    if (myNewPart == NULL) {
	free(displs);
	free(myPart);
	Oom();
    }

    /*
     * Offset and length of the part of the image to load
     * in the local process, including the additional
     * row at the top and/or the bottom.
     */
    *offset = self == 0 ? 0 : (displs[self]-columns);
    if (np == 1)
	*length = myRows * columns;
    else
	*length = (myRows + (self == 0 || self == np-1 ? 1 : 2)) * columns;

    if (debugOutput) {
        fprintf(stderr, "self=%d: *offset = %d, *length = %d\n",
                self, *offset, *length);
    }

    /* Add zeros in top row in process 0 and in bottom row in process np-1. */
    prepare_myPart(columns);

    return (self == 0 ? &myPart[columns] : myPart);
}

void usage(const char *progname) {
    if (self == 0) {
	fprintf(stderr,
		"USAGE: %s -i input.pgm [-o output.pgm] [-c coeff] "
		"[-n] [-r] [-t] [-d] [-h]\n"
		"  coeff  sobel coefficient\n"
		"  -n     use naive Sobel operator\n"
		"  -d     give some debug output\n"
		"  -h     print this help\n",
		progname);
    }
}

int main(int argc, char *argv[])
{
    int option;
    uint8_t *image;
    char *filename, *outfilename;
    enum pnm_kind kind;
    int rows, columns;
    bool naive = false;     /* use naive implementation */
    double time_start, time_loaded;
    double time_computed=0.0, time_finished;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &self);

    filename = NULL;
    outfilename = NULL;
    sobelC = 0.9;
    debugOutput = false;

    while ((option = getopt(argc,argv,"i:o:c:ndh")) != -1) {
	switch(option) {
	case 'i': filename = optarg; break;
	case 'o': outfilename = optarg; break;
	case 'c': sobelC = atof(optarg); break;
	case 'n': naive = true; break;
	case 'd': debugOutput = true; break;
	case 'h':
	    usage(argv[0]);
	    MPI_Finalize();
	    return 0;
	default:
	    usage(argv[0]);
	    MPI_Finalize();
	    return 1;
	}
    }
    if (filename == NULL) {
        usage(argv[0]);
        MPI_Finalize();
	return 1;
    }

    time_start = seconds();
    image = ppp_pnm_read_part(filename, &kind, &rows, &columns, &maxcolor,
			      partFn);
    if (image == NULL) {
	fprintf(stderr, "Could not load image from file '%s'"
		" on processor %d.\n", filename, self);
	MPI_Abort(MPI_COMM_WORLD, 1);
    } else if (kind != PNM_KIND_PGM) {
	if (self == 0)
	    fprintf(stderr, "Image is not a \"portable graymap.\"\n");
	MPI_Abort(MPI_COMM_WORLD, 1);
    }

    time_loaded = seconds();
    
    if (naive)
        sobelNaive(columns);
    else
        sobel(columns);

    time_computed = seconds();

    collect(columns);
    
    time_finished = seconds();

    if (self == 0) {
	if (outfilename != NULL)
	    ppp_pnm_write(outfilename, kind, rows, columns, maxcolor,
			  myNewPart);

	printf("Times:\n");
	printf("  Load:    %.6f s\n", time_loaded-time_start);
        printf("  Compute: %.6f s\n", time_computed-time_loaded);
        printf("  Collect: %.6f s\n", time_finished-time_computed);
	printf("  TOTAL:   %.6f s\n", time_finished-time_start);
    }

    free(myPart);
    MPI_Finalize();

    return 0;
}
