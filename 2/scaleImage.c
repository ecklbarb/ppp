/*
 * Beispiel fuer die Benutzung der ppp_pnm Bibliothek.
 * Kompilieren mit:
 *
 *     gcc -std=c99 -Wall -o invert-pgm invert-pgm.c
 *          -I/home/ppp2015/ppp_pnm
 *          -L/home/ppp2015/ppp_pnm
 *          -lppp_pnm
 *
 * Die Quellen der ppp_pnm Bibliothek koennen aus /home/ppp2015/ppp_pnm
 * kopiert oder mittels
 *
 *     git clone /home/ppp2015/repos/ppp_pnm.git
 *
 * ausgecheckt werden.
 */
#define _POSIX_C_SOURCE 2
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

#include <unistd.h>


#include "ppp_pnm.h"
#include "mpi.h"
#include "omp.h"

int np, self;

/* skaliert einen Bildwert anhand der Vorgaben */
static int scale( int value, int maxcolor, int mincolor, int min, int max) {
  int result;
  result = (value - mincolor) * (max - min) + (maxcolor - mincolor) / 2;
  return result / (maxcolor - mincolor) + min;
}

/* liefert die Sekunden seit dem 01.01.1970 */
static double seconds() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
}

/*
 * Load a PGM (Portable Graymap) image and scale
 * the gray values of every pixel.
 * The program is called with 2 arguments:
 *      -m min-value -n max-value -[s|o|i] Input-image  Output-image
 */
int main(int argc, char *argv[]) {
    enum pnm_kind kind;
    int rows = 0;
    int columns = 0;
    int interval, min, max;
  int  maxcolor = 0;
  int mincolor = 255; /* altes Minimum & Maximum*/
  double size, starttime; 
  uint8_t *image, *rcvbuf;
    int x, y;
    int option;
    /*printf("#Args: %d, argv[1]: %s\n", argc, argv[1]);*/
    if (argc < 7) {
	fprintf(stderr, "USAGE: %s IN OUT\n", argv[0]);
	return 1;
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &self);

    /* Beispiel fuer Kommandozeilen-Optionen mit getopt.
     * Ein nachgestellter Doppelpunkt signalisiert eine
     * Option mit Argument (in diesem Beispiel bei "-c").
     */
    int  method;
    bool min_val, max_val, seq, mpi, omp;
    min_val = max_val = seq = mpi= omp = false;
    /*for( z = 0; z < argc; z++) {
      printf("%s |", argv[z]);
      }*/
    while ((option = getopt(argc,argv,"m:n:soi")) != -1) {
        switch(option) {
        case 'm': min_val = true; min = atoi(optarg); break;
        case 'n': max_val = true; max = atoi(optarg); break;
        case 's': seq = true; method = 'S'; break;
	case 'o': omp = true; method = 'O'; break;
	case 'i': mpi = true; method = 'M'; break;
        default:
            if (self == 0)
                fprintf(stderr, "Option error\n");
            MPI_Finalize();
            return 1;        
	}
    }

    /* Ausgabe der gesetzten Argumente 
    if (min_val)
      printf("Option -m mit Argument %d gesetzt min \n", min);
    if (max_val)
      printf("Option -n mit Argumten %d gesetzt max\n", max);
    if (repition)
      printf("Option -r mit Argument %d gesetzt die Anzahl an Wiederholungen\n", repit);
    if (source && num_method < 3)
      printf("Option -s mit Argument %d gesetzt setzt den Quellprozess\n", src);
    */

    /*
     * Load the image (name in argv[1]),
     * store the kind (PBM, PGM, PPM) in 'kind',
     * the number of rows and columns in 'rows' and 'columns',
     * the maximal gray value of the image format (NOT the
     * maximal gray value used in the image) in 'maxcolor' and return
     * the image row-wise with one int per pixel.
     */
    double *times = (double *)malloc(5 * sizeof(double));
    if (self == 0) image = ppp_pnm_read(argv[argc - 2], &kind, &rows, &columns, &maxcolor);
    if (image != NULL || self != 0) {
	if (kind == PNM_KIND_PGM || self != 0) {
	  switch (method) {

	  case 'S':
	    if (self == 0) {
	      starttime = seconds();
	    for (y=0; y<rows; y++) {
	      for (x=0; x<columns; x++) {
		  
		if (image[y*columns+x] > maxcolor) {
		  maxcolor = image[y*columns+x];
		}
		if (image[y*columns+x] < mincolor) {
		  mincolor = image[y*columns+x];
		}
	      }
	    }
	    for (y=0; y<rows; y++) {
	      for (x=0; x<columns; x++) {
		image[y*columns+x] = scale(image[y*columns+x], maxcolor, mincolor, min, max);
	      }
	    }
	    printf("Processtime: %f\n", (seconds() - starttime));
	    /*
	     * Save the image, parameters are analogous to
	     * ppp_pnm_read (but kind, rows, columns, maxcolor are
	     * not passed as pointers for ppp_pnm_write). The
	     * last parameter is a pointer to the image to be saved.
	     */
	    if (ppp_pnm_write(argv[argc - 1], kind, rows, columns, maxcolor, image) != 0)
	      fprintf(stderr, "write error\n");
	    printf("Time with storage: %f\n", (seconds() - starttime));
	    }
	    break;
	  case 'O':
		starttime = seconds();
		#pragma omp parallel
		{
		int maxc = 0;
		int minc = 255;

/*
		#pragma omp for private(x) reduction(max : maxcolor) reduction(min : mincolor)
	     	for (y=0; y<rows; y++) {
	      	   for (x=0; x<columns; x++) {
			mincolor = fmin(mincolor, image[y*columns+x]);
			maxcolor = fmax(maxcolor, image[y*columns+x]);
		   }
	        }
*/
		#pragma omp for private(x)
	     	for (y=0; y<rows; y++) {
	      	   for (x=0; x<columns; x++) {
			minc = fmin(minc, image[y*columns+x]);
			maxc = fmax(maxc, image[y*columns+x]);
		   }
	        }

		# pragma omp critical
		{
			maxcolor = fmax(maxcolor, maxc);
			mincolor = fmin(mincolor, minc);
		}

			
	    	/*printf("maxcolor: %d, mincolor: %d, neue: max: %d , min: %d \n", maxcolor, mincolor, max, min);*/

		# pragma omp for private(x)
	    	for (y=0; y<rows; y++) {
	      	    for (x=0; x<columns; x++) {
			image[y*columns+x] = scale(image[y*columns+x], maxcolor, mincolor, min, max);
	      	     }
	    	}
		}

      		printf("Process: %d, Time: %f s\n", self, seconds()-starttime);

	    /*
	     * Save the image, parameters are analogous to
	     * ppp_pnm_read (but kind, rows, columns, maxcolor are
	     * not passed as pointers for ppp_pnm_write). The
	     * last parameter is a pointer to the image to be saved.
	     */
		if (self == 0) {
	    if (ppp_pnm_write(argv[argc - 1], kind, rows, columns, maxcolor, image) != 0)
	      fprintf(stderr, "write error\n");
		}
		break;
	  case 'M':
	    starttime = seconds();
	    MPI_Bcast(&rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
	    MPI_Bcast(&columns, 1, MPI_INT, 0, MPI_COMM_WORLD);
	    printf("Time for bcast: %f, values: %d %d, ", seconds() - starttime, rows, columns);
	    size = rows * columns;
	    interval =(int)ceil(size/np);
	    rcvbuf = (uint8_t *)malloc(interval*sizeof(uint8_t));
	    int *offsets, *sizes, i;
	    offsets = (int *)malloc(np*sizeof(int));
	    sizes = (int *)malloc(np*sizeof(int));
	      for (i = 0; i < np; i++) {
		sizes[i] = interval;
		offsets[i] = i * interval;
		sizes[np - 1] = size - (np - 1) * interval;
	      }
	     MPI_Scatterv(image, sizes, offsets, MPI_UINT8_T, rcvbuf, sizes[self], MPI_UINT8_T, 0, MPI_COMM_WORLD);
	     printf("Time after Scatter: %f\n", seconds() - starttime);	    
maxcolor = 0; /* ist beim Laden standardm��ig 255 */
	    for(i = 0; i < interval; i++) {
	      mincolor = fmin(mincolor, rcvbuf[i]);
	      maxcolor = fmax(maxcolor, rcvbuf[i]);
	    }
	    MPI_Allreduce(&mincolor, &mincolor, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
	    MPI_Allreduce(&maxcolor, &maxcolor, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
	    int arraysize = interval;
	    if ((self + 1) * interval >= size) arraysize = size - self * interval;

	    uint8_t *transfer = (uint8_t *)malloc(interval*sizeof(uint8_t));
	    for(i = 0; i < interval; i++) {
	      if (self * interval + i < size) {
		transfer[i] = scale(rcvbuf[i], maxcolor, mincolor, min, max);
	      }
	    }
	    /*offsets = (int *)malloc(np*sizeof(int));
	      sizes = (int *)malloc(np*sizeof(int));*/

	    for (i = 0; i < np; i++) {
	      offsets[i] = i * interval;
	      sizes[i] = interval;
	    }
	     sizes[np - 1] = size - (np-1) * interval;

	    MPI_Gatherv(transfer, arraysize, MPI_UINT8_T, image, sizes, offsets, MPI_UINT8_T, 0, MPI_COMM_WORLD);
	    printf("Process: %d, Processtime: %f\n", self, seconds() - starttime);
	    times[i] = seconds() - starttime;
	    if (self == 0) {
	      if (ppp_pnm_write(argv[argc-1], kind, rows, columns, max, image) != 0)
	        fprintf(stderr, "write error\n");
	    }
	    break;

	  default: printf("Keine Methode ausgew�hlt");

	  }
	  } else
	    fprintf(stderr, "not a PGM image\n");
        
    } else
	fprintf(stderr, "could not load image\n");
    

    /*starttime = 0; 
    for(int i = 0; i <  5; i++) {
      starttime += times[i];
      printf("Time: %f\n", times[i]);
    }
    printf("Average time: %f", starttime/5);*/
    MPI_Finalize();
    return 0;
}
