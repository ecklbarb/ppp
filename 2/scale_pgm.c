/*
 * Beispiel fuer die Benutzung der ppp_pnm Bibliothek.
 * Kompilieren mit:
 *
 *     gcc -std=c99 -Wall -o invert-pgm invert-pgm.c
 *          -I/home/ppp2015/ppp_pnm
 *          -L/home/ppp2015/ppp_pnm
 *          -lppp_pnm
 *
 * Die Quellen der ppp_pnm Bibliothek koennen aus /home/ppp2015/ppp_pnm
 * kopiert oder mittels
 *
 *     git clone /home/ppp2015/repos/ppp_pnm.git
 *
 * ausgecheckt werden.
 */
#define _POSIX_C_SOURCE 2
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <unistd.h>

#include "ppp_pnm.h"
#include "mpi.h"

int np, self;
int min, max; /*neues Minimum & Maximum*/

/* skaliert einen Bildwert anhand der Vorgaben */
static int scale( int value, int maxcolor, int mincolor) {
  int result;
  result = (value - mincolor) * (max - min) + (maxcolor - mincolor) / 2;
  return result / (maxcolor - mincolor) + min;
}

/* liefert die Sekunden seit dem 01.01.1970 */
static double seconds() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
}

/*
 * Load a PGM (Portable Graymap) image and invert
 * the gray values of every pixel.
 * The program is called with 2 arguments:
 *      Input-image  Output-image
 */
int main(int argc, char *argv[]) {
    enum pnm_kind kind;
   
    int rows, columns;
 int  maxcolor = 0;
int mincolor = 255; /* altes Minimum & Maximum*/
 uint8_t *image, *rbuf;
    int x, y;
    int option;
    //printf("#Args: %d, argv[1]: %s\n", argc, argv[1]);
    if (argc < 3) {
	fprintf(stderr, "USAGE: %s IN OUT\n", argv[0]);
	return 1;
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &self);

    /* Beispiel fuer Kommandozeilen-Optionen mit getopt.
     * Ein nachgestellter Doppelpunkt signalisiert eine
     * Option mit Argument (in diesem Beispiel bei "-c").
     */
    int  method;
    method = 'M';
    char resource[10], target[10];
    bool min_val, max_val, seq, mpi, omp;
    min_val = max_val = seq = mpi= omp = false;
    int z;
    /*for( z = 0; z < argc; z++) {
      printf("%s |", argv[z]);
      }
*/
    while ((option = getopt(argc,argv,"m:n:soi")) != -1) {
	printf("Option %d and value = %d \n", option, atoi(optarg));
        switch(option) {
        case 'm': min_val = true; min = atoi(optarg); break;
        case 'n': max_val = true; max = atoi(optarg); break;
        case 's': seq = true; method = 'S'; break;
	case 'o': omp = true; method = 'O'; break;
	case 'i': mpi = true; method = 'M'; break;
        default:
            if (self == 0)
                fprintf(stderr, "Option error\n");
            MPI_Finalize();
            return 1;        
	}
    }

    /* Ausgabe der gesetzten Argumente */
    if (min_val)
      printf("Option -m mit Argument %d gesetzt min \n", min);
    if (max_val)
      printf("Option -n mit Argumten %d gesetzt max\n", max);
    /*if (repition)
      printf("Option -r mit Argument %d gesetzt die Anzahl an Wiederholungen\n", repit);
    if (source && num_method < 3)
      printf("Option -s mit Argument %d gesetzt setzt den Quellprozess\n", src);
    */

    min = 30; max = 90;
    /*
     * Load the image (name in argv[1]),
     * store the kind (PBM, PGM, PPM) in 'kind',
     * the number of rows and columns in 'rows' and 'columns',
     * the maximal gray value of the image format (NOT the
     * maximal gray value used in the image) in 'maxcolor' and return
     * the image row-wise with one int per pixel.
     */
    image = ppp_pnm_read(argv[1], &kind, &rows, &columns, &maxcolor);
    if (image != NULL) {
	if (kind == PNM_KIND_PGM) {
	  double size;
	  switch (method) {

	  case 'S':
	    for (y=0; y<rows; y++) {
	      for (x=0; x<columns; x++) {
		  
		if (image[y*columns+x] > maxcolor) {
		  maxcolor = image[y*columns+x];
		}
		if (image[y*columns+x] < mincolor) {
		  mincolor = image[y*columns+x];
		}
	      }
	    }
	    printf("\nmax: %d, min: %d, neue: %d %d", maxcolor, mincolor, max, min);
	    for (y=0; y<rows; y++) {
	      for (x=0; x<columns; x++) {
		image[y*columns+x] = scale(image[y*columns+x], maxcolor, mincolor);
	      }
	    }

	    /*
	     * Save the image, parameters are analogous to
	     * ppp_pnm_read (but kind, rows, columns, maxcolor are
	     * not passed as pointers for ppp_pnm_write). The
	     * last parameter is a pointer to the image to be saved.
	     */
	    if (ppp_pnm_write(argv[2], kind, rows, columns, maxcolor, image) != 0)
	      fprintf(stderr, "write error\n");
	    break;
	  case 'O':break;
	  case 'M':
	    maxcolor = 0; /* ist beim Laden standardm��ig 255 */
	    size = rows * columns;
	    int interval = (int) (ceil(size / np));
	    int i;
	    for(i = self * interval; i < (self + 1) * interval; i++) {
	      if (image[i] < mincolor) mincolor = image[i];
	      if (image[i] > maxcolor) maxcolor = image[i];
	    }
	    printf("MaxStart: %d ||", maxcolor);
	    MPI_Allreduce(&mincolor, & mincolor, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
	    MPI_Allreduce(&maxcolor, & maxcolor, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
	    printf("ProcessNr: %d, min %d, max %d", self, mincolor, maxcolor);
	    for(i = self * interval; i < (self + 1) * interval; i++) {
	      if (i < size) {
		image[i] = scale(image[i], maxcolor, mincolor);
	      }
	    }
	    image[self * interval] = 42;
	    if ((self + 1) * interval >= size) interval = size - self * interval;
	    int *offsets, *sizes;
	    offsets = (int *)malloc(np*sizeof(int));
	    sizes = (int *)malloc(np*sizeof(int));
	    for (i = 0; i < np; i++) {
	      offsets[i] = i * interval;
	      sizes[i] = interval;
	    }
	    rbuf = (uint8_t *)malloc(size);
	    if (self == np-1) 
	      sizes[np - 1] = size - (np-1) * interval;
	    if (self != 0)
	    MPI_Gatherv(image, interval, MPI_UINT8_T, rbuf, sizes, offsets, MPI_UINT8_T, 0, MPI_COMM_WORLD);
	    for (i = 0; i < np; i++) 
	      printf("|%d", image[i*interval]);
 if (ppp_pnm_write(argv[2], kind, rows, columns, maxcolor, rbuf) != 0)
	      fprintf(stderr, "write error\n");

	    break;

	  default: printf("Keine Methode ausgew�hlt");

	  }
	} else
	    fprintf(stderr, "not a PGM image\n");

	free(image);
    } else
	fprintf(stderr, "could not load image\n");
    printf("\n");
    return 0;
}
