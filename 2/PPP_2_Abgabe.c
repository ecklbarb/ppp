/*
 * Beispiel fuer die Benutzung der ppp_pnm Bibliothek.
 * Kompilieren mit:
 *
 *     gcc -std=c99 -Wall -o invert-pgm invert-pgm.c
 *          -I/home/ppp2015/ppp_pnm
 *          -L/home/ppp2015/ppp_pnm
 *          -lppp_pnm
 *
 * Die Quellen der ppp_pnm Bibliothek koennen aus /home/ppp2015/ppp_pnm
 * kopiert oder mittels
 *
 *     git clone /home/ppp2015/repos/ppp_pnm.git
 *
 * ausgecheckt werden.
 */
#define _POSIX_C_SOURCE 2
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

#include <unistd.h>


#include "ppp_pnm.h"
#include "mpi.h"
#include "omp.h"

int np, self;

/* skaliert einen Bildwert anhand der Vorgaben */
static int scale( int value, int maxcolor, int mincolor, int min, int max) {
  int result;
  result = (value - mincolor) * (max - min) + (maxcolor - mincolor) / 2;
  return result / (maxcolor - mincolor) + min;
}

/* liefert die Sekunden seit dem 01.01.1970 */
static double seconds() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
}

/*
 * Load a PGM (Portable Graymap) image and scale
 * the gray values of every pixel due to the new values.
 * The program is called with 5 arguments:
 *  -m new_min -n new_max -[s|o|i]  Input-image  Output-image
 */
int main(int argc, char *argv[]) {
    enum pnm_kind kind;
    int rows = 0;
    int columns = 0;
    int  min, max, x, y, option;
    int  maxcolor = 0;
    int mincolor = 255; /* altes Minimum & Maximum*/
    double starttime; 
    uint8_t *image;
    if (argc < 7) {
	fprintf(stderr, "USAGE: %s IN OUT\n", argv[0]);
	return 1;
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &self);

    /* Beispiel fuer Kommandozeilen-Optionen mit getopt.
     * Ein nachgestellter Doppelpunkt signalisiert eine
     * Option mit Argument (in diesem Beispiel bei "-m").
     */
    int  method = 'S';
    bool min_val, max_val, seq, mpi, omp;
    min_val = max_val = seq = mpi= omp = false;
    while ((option = getopt(argc,argv,"m:n:soi")) != -1) {
        switch(option) {
        case 'm': min_val = true; min = atoi(optarg); break;
        case 'n': max_val = true; max = atoi(optarg); break;
        case 's': seq = true; method = 'S'; break;
	case 'o': omp = true; method = 'O'; break;
	case 'i': mpi = true; method = 'M'; break;
        default:
            if (self == 0)
                fprintf(stderr, "Option error\n");
            MPI_Finalize();
            return 1;        
	}
    }
    if (!min_val || !max_val || !(seq || omp || mpi)) {
      fprintf(stderr, "Not enough options chosen.\n");
      MPI_Finalize();     
      return 2;
    }

    if (min < 0 || min > 255 || max <= 0 || max > 255 ) {
      fprintf(stderr, "Invalid min/max parameter.\n");
      MPI_Finalize();     
      return 3;
    }
    if (min > max && self == 0) printf("The picture colors are now inverted.\n");

    /*
     * Load the image (name in argv[1]),
     * store the kind (PBM, PGM, PPM) in 'kind',
     * the number of rows and columns in 'rows' and 'columns',
     * the maximal gray value of the image format (NOT the
     * maximal gray value used in the image) in 'maxcolor' and return
     * the image row-wise with one int per pixel.
     */
    MPI_Barrier(MPI_COMM_WORLD);
    starttime = seconds();
    if (self == 0) {
      image = ppp_pnm_read(argv[argc - 2], &kind, &rows, &columns, &maxcolor);
      printf("Time for reading image: %f\n", seconds() - starttime);
    }
    if (image != NULL || self != 0) {
	if (kind == PNM_KIND_PGM || self != 0) {
	  switch (method) {

	  case 'S':
	    if (self == 0) {
	      starttime = seconds();
	    for (y=0; y<rows; y++) {
	      for (x=0; x<columns; x++) {
		  
		if (image[y*columns+x] > maxcolor) {
		  maxcolor = image[y*columns+x];
		}
		if (image[y*columns+x] < mincolor) {
		  mincolor = image[y*columns+x];
		}
	      }
	    }
	    printf("Time after min/max: %f\n", (seconds() - starttime));

	    for (y=0; y<rows; y++) {
	      for (x=0; x<columns; x++) {
		image[y*columns+x] = scale(image[y*columns+x], maxcolor, mincolor, min, max);
	      }
	    }
	    printf("Processtime: %f\n", (seconds() - starttime));
	    /*
	     * Save the image, parameters are analogous to
	     * ppp_pnm_read (but kind, rows, columns, maxcolor are
	     * not passed as pointers for ppp_pnm_write). The
	     * last parameter is a pointer to the image to be saved.
	     */
	    if (ppp_pnm_write(argv[argc - 1], kind, rows, columns, maxcolor, image) != 0)
	      fprintf(stderr, "write error\n");
	      printf("Time with storage: %f\n", (seconds() - starttime));
	    }
	    break;
	  case 'O':
	    if (rows * columns < omp_get_num_threads()) {
	      fprintf(stderr, "Number of threads higher than amount of pixels.\n");
	      MPI_Finalize();
	      return 4;
	    }
	    maxcolor = 0;
		#pragma omp parallel
		{
		#pragma omp for private(x) reduction(max : maxcolor) reduction(min : mincolor)
	     	for (y=0; y<rows; y++) {
	      	   for (x=0; x<columns; x++) {
		  	maxcolor = fmax(maxcolor, image[y*columns+x]);
			mincolor = fmin(mincolor, image[y*columns+x]);
		   }
	        }
		if (omp_get_thread_num() == 0) 
		  printf("Time after min/max: %f\n", (seconds() - starttime));
	        
		# pragma omp for private(x)
	    	for (y=0; y<rows; y++) {
	      	    for (x=0; x<columns; x++) {
			image[y*columns+x] = scale(image[y*columns+x], maxcolor, mincolor, min, max);
	      	     }
	    	}
		if (omp_get_thread_num() == 0) 
		  printf("Process: %d, Time after scale: %f s\n", self, seconds()-starttime);
		}

	    /*
	     * Save the image, parameters are analogous to
	     * ppp_pnm_read (but kind, rows, columns, maxcolor are
	     * not passed as pointers for ppp_pnm_write). The
	     * last parameter is a pointer to the image to be saved.
	     */
		if (self == 0) {
		  if (ppp_pnm_write(argv[argc - 1], kind, rows, columns, maxcolor, image) != 0)
		    fprintf(stderr, "write error\n");
		}
		break;
	  case 'M':
	    MPI_Bcast(&rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
	    MPI_Bcast(&columns, 1, MPI_INT, 0, MPI_COMM_WORLD);
	    if (rows * columns < np)  {
		  if (self == 0) fprintf(stderr, "Number of processes higher than amount of pixels.\n");
	      MPI_Finalize();
	      return 5;
	    }
	    printf("Time after bcast: %f\n ", seconds() - starttime);

	    int size = rows * columns;
	    int interval =(int)ceil(size/np);
	    uint8_t *rcvbuf = (uint8_t *)malloc(interval*sizeof(uint8_t));
	    int *offsets, *sizes;
	    offsets = (int *)malloc(np*sizeof(int));
	    sizes = (int *)malloc(np*sizeof(int));
	    
	    for (int i = 0; i < np; i++) {
	      sizes[i] = interval;
	      offsets[i] = i * interval;
	      sizes[np - 1] = size - (np - 1) * interval;
	    }
	    MPI_Scatterv(image, sizes, offsets, MPI_UINT8_T, rcvbuf, sizes[self], MPI_UINT8_T, 0, MPI_COMM_WORLD);
	    printf("Time after Scatter: %f\n", seconds() - starttime);	    

	    maxcolor = 0; /* ist beim Laden standardm��ig 255 */

            #pragma omp parallel for reduction (min : mincolor) reduction (max : maxcolor)
	    for(int i = 0; i < interval; i++) {
	      mincolor = fmin(mincolor, rcvbuf[i]);
	      maxcolor = fmax(maxcolor, rcvbuf[i]);
	    }
	    printf("Time after min/max: %f\n", seconds() - starttime);

	    MPI_Allreduce(&mincolor, &mincolor, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
	    printf("Time after first reduce: %f\n", seconds() - starttime);
	    MPI_Allreduce(&maxcolor, &maxcolor, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
	    printf("Time after second reduce: %f\n", seconds() - starttime);
   
            #pragma omp parallel for
	    for(int i = 0; i < interval; i++) {
	      if (self * interval + i < size) {
		rcvbuf[i] = scale(rcvbuf[i], maxcolor, mincolor, min, max);
	      }
	    }
	    printf("Time after scaling: %f\n", seconds() - starttime);

	    for (int i = 0; i < np; i++) {
	      offsets[i] = i * interval;
	      sizes[i] = interval;
	    }
	    sizes[np - 1] = size - (np-1) * interval;

	    MPI_Gatherv(rcvbuf, sizes[self], MPI_UINT8_T, image, sizes, offsets, MPI_UINT8_T, 0, MPI_COMM_WORLD);
	    printf("Process: %d, time after gather: %f\n", self, seconds() - starttime);
	    if (self == 0) {
	      if (ppp_pnm_write(argv[argc-1], kind, rows, columns, fmax(min, max), image) != 0)
	        fprintf(stderr, "write error\n");
	     printf("Time after writing image: %f\n", seconds() - starttime);
	    }
	    printf("\n");
	    break;
	  default: printf("Keine Methode ausgew�hlt");
	  }
       	} else 
	   fprintf(stderr, "not a PGM image\n");
      } else 
	  fprintf(stderr, "could not load image\n");
	
    MPI_Finalize();
    return 0;
}
