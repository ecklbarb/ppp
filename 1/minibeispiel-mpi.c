#include <stdio.h>
#include <omp.h>
#include "mpi.h"

int main(int argc, char *argv[])
{
    MPI_Comm comm = MPI_COMM_WORLD;
    int np, self;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(comm, &np);
    MPI_Comm_rank(comm, &self);

    printf("Hier ist Prozess #%d von %d insgesamt.\n", self, np);

#pragma omp parallel
    {
	int np_omp   = omp_get_num_threads();
	int self_omp = omp_get_thread_num();
	printf("Hier ist Thread #%d von %d Threads in Prozess %d insgesamt.\n", self_omp, np_omp, self);
    }

    MPI_Finalize();

    return 0;
}
