#define _POSIX_C_SOURCE 2
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <math.h>

#include "mpi.h"

int np;   /* Anzahl der MPI-Prozesse */
int self; /* Nummer des eigenen Prozesses (im Bereich 0,...,np-1) */

/* liefert die Sekunden seit dem 01.01.1970 */
static double seconds() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + ((double)tv.tv_usec)/1000000.0;
}

/* liefert den zweier Logarithmus von der Eingabe*/
static  double logerithmus2(double x) { 
  return log10(x)/log10(2);
}

/*
 * Speicherplatz fuer `size' viele int anfordern (d.h. ein
 * int-Array der Laenge `size').
 * Der Speicherplatz ist nach Benutzung mit  free  freizugeben:
 *    int *p = allocints(100);
 *    if (p != NULL) {
 *      .... // p benutzen
 *      free(p);
 *    }
 */
static int *allocints(int size) {
    int *p;
    p = (int *)malloc(size * sizeof(int));
    return p;
}

int main(int argc, char *argv[])
{
    double start, end;
    int option;

    bool array_size, method, repition, source;

    /* size ist die Größe des Arrays, das verschickt werden soll
     * repit ist die Anzahl der Wiederholungen, die durchgeführt werden sollen
     * src ist der Startprozess bei Verfahren 1 und 2
     * num_method gibt die Nummer des Verfahres an, das ausgeführt werden soll
     * wobei 1 der native MPI Broadcast, 2 das explizite Senden von einem angegebenen Quellprozess
     * und 3 das baumartige Verteilen ist mit dem Quellprozess 0
     */
    int size = 2, repit = 1, src = 0 , num_method = 3;

    /* MPI initialisieren und die Anzahl der Prozesse sowie
     * die eigene Prozessnummer bestimmen.
     */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    MPI_Comm_rank(MPI_COMM_WORLD, &self);

    /* Beispiel fuer Kommandozeilen-Optionen mit getopt.
     * Ein nachgestellter Doppelpunkt signalisiert eine
     * Option mit Argument (in diesem Beispiel bei "-c").
     */
    array_size = method = repition = source = false;
    while ((option = getopt(argc,argv,"a:m:r:s:")) != -1) {
        switch(option) {
        case 'a': array_size = true; size = atoi(optarg); break;
        case 'm': method = true; num_method = atoi(optarg); break;
        case 'r': repition = true; repit = atoi(optarg); break;
	case 's': source = true; src = atoi(optarg); break;
        default:
            if (self == 0)
                fprintf(stderr, "Option error\n");
            MPI_Finalize();
            return 1;        
	}
    }

    /* Ausgabe der gesetzten Argumente */
    if (array_size)
      printf("Option -a mit Argument %d gesetzt die Größe des Arrays \n", size);
    if (method)
      printf("Option -m mit Argumten %d gesetzt die Nummer des Verfahrens\n", num_method);
    if (repition)
      printf("Option -r mit Argument %d gesetzt die Anzahl an Wiederholungen\n", repit);
    if (source && num_method < 3)
      printf("Option -s mit Argument %d gesetzt setzt den Quellprozess\n", src);

    /* hier geht's los... */
    int i;
    for (i = 0; i < repit; i++) {
      int *data = allocints(size);
      if (num_method == 3) {
	if(self == 0)
	  data[0] = 42;
	else
	  data[0] = 0;
      } else {
	if(self == src)
	  data[0] = 42;
	else
	  data[0] = 0;
      }

      printf("rank = %d, size = %d\n", self, np);
      printf("In process %d: data[0]= %d\n", self, data[0]);
    
      MPI_Barrier(MPI_COMM_WORLD);
      start = seconds();
      switch (num_method) {
      case 1:
	MPI_Bcast(data, size, MPI_INT, src, MPI_COMM_WORLD);
	break;
      case 2:
	if (self == src) {
	  int dest;
	  for (dest = 0; dest < np; dest++) {
	    if (dest != src) {
	      MPI_Request req;
	      MPI_Isend(data, size, MPI_INT, dest, 0, MPI_COMM_WORLD, &req);
	      /* Alternativ MPI_Send ohne Request */
	    }
	  }
	}
	else {
	  MPI_Recv(data, size, MPI_INT, src, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
	break;
      case 3: {
	int l;
	for (l = 0; l < ceil(logerithmus2((double) np)); l++) {
	  int j;
	  for (j = 0; j < pow(2,l) ; j++) {
	    if (self == j && (pow(2,l) +j) < np)
	      MPI_Send(data, size, MPI_INT, (pow(2,l) + j), 0, MPI_COMM_WORLD);
	    else if (self == pow(2, l) + j)
	      MPI_Recv(data, size, MPI_INT, j, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	  }
	  printf("In process %d after iteration %d: data[0]= %d \n", self, l, data[0]);
	}
	break;
      }
      default:
	return 1;
      }
      printf("In process %d: data[0]= %d \n", self, data[0]);

      MPI_Barrier(MPI_COMM_WORLD);
      end = seconds();
      printf("Time: %f s\n", end-start);
      free(data);
    }

    
    /* MPI beenden */
    MPI_Finalize();

    return 0;
}
